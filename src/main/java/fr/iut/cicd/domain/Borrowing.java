package fr.iut.cicd.domain;

import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

import java.util.Date;

@Data
@Accessors(chain = true)
@MongoEntity(collection = "borrowings")
public class Borrowing {
    @BsonId
    private ObjectId id;
    private Date at;
    private Date returnedAt;
    private String userId;
    private Book book;

}
