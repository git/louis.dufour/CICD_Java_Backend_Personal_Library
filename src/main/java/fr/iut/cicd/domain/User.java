package fr.iut.cicd.domain;

import io.quarkus.hibernate.orm.panache.PanacheEntity;
import jakarta.persistence.Entity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@Entity(name = "users")
@EqualsAndHashCode(callSuper = true)
public class User extends PanacheEntity {
    private String username;
    private String password;
    private String role;
}
