package fr.iut.cicd.domain;

import io.quarkus.mongodb.panache.common.MongoEntity;
import lombok.Data;
import lombok.experimental.Accessors;
import org.bson.codecs.pojo.annotations.BsonId;
import org.bson.types.ObjectId;

@Data
@Accessors(chain = true)
@MongoEntity(collection = "library")
public class LibraryEntry {
    @BsonId
    private ObjectId id;
    private String userId;
    private Book book;

}
