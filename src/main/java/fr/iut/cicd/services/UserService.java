package fr.iut.cicd.services;

import fr.iut.cicd.domain.User;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.transaction.Transactional;

@ApplicationScoped
public class UserService {

    @Transactional
    public void create(String username, String password) {
        var user = new User()
                .setRole("user")
                .setPassword(password)
                .setUsername(username);
        user.persistAndFlush();
    }
}
