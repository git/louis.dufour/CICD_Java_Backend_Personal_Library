package fr.iut.cicd.services;

import fr.iut.cicd.domain.LibraryEntry;
import fr.iut.cicd.repository.LibraryEntryRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bson.types.ObjectId;

import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class LibraryService {

    @Inject
    LibraryEntryRepository repository;

    public LibraryEntry create(LibraryEntry domain, String connectedUsername) {
        domain.setId(new ObjectId())
                .setUserId(connectedUsername);
        repository.persist(domain);
        return domain;
    }

    public LibraryEntry update(LibraryEntry domain, String connectedUsername) {
        var existing = repository.findById(domain.getId());
        if (existing != null && Objects.equals(existing.getUserId(), connectedUsername)) {
            repository.update(domain);
            existing = repository.findById(domain.getId());
        }
        return existing;
    }


    public List<LibraryEntry> list(String connectedUsername) {
        return repository.findAllByUsername(connectedUsername);
    }

    public void delete(ObjectId oid, String connectedUsername) {
        var existing = repository.findById(oid);
        if (existing != null && Objects.equals(existing.getUserId(), connectedUsername)) {
            repository.deleteById(oid);
        }
    }
}
