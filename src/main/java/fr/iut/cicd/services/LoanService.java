package fr.iut.cicd.services;

import fr.iut.cicd.domain.Book;
import fr.iut.cicd.domain.Loan;
import fr.iut.cicd.repository.LoanRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class LoanService {
    @Inject
    LoanRepository repository;

    public Loan create(Book book, Date at, String to, String name) {
        var loan = new Loan()
                .setId(new ObjectId())
                .setBook(book)
                .setTo(to)
                .setUserId(name)
                .setAt(at == null ? new Date() : at);
        repository.persist(loan);
        return repository.findById(loan.getId());
    }

    public List<Loan> list(String username) {
        return repository.findAllByUsername(username);
    }

    public Loan returned(ObjectId loanId, Date dateToUse, String username) {
        var currentLoan = repository.findById(loanId);
        if (currentLoan != null && Objects.equals(currentLoan.getUserId(), username)) {
            currentLoan.setReturnedAt(dateToUse);
            repository.update(currentLoan);
            currentLoan = repository.findById(loanId);
        }
        return currentLoan;
    }
}
