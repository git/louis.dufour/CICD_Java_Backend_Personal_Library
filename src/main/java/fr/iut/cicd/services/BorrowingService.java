package fr.iut.cicd.services;

import fr.iut.cicd.domain.Book;
import fr.iut.cicd.domain.Borrowing;
import fr.iut.cicd.repository.BorrowingRepository;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import org.bson.types.ObjectId;

import java.util.Date;
import java.util.List;
import java.util.Objects;

@ApplicationScoped
public class BorrowingService {
    @Inject
    BorrowingRepository repository;

    public Borrowing create(Book book, Date at, String name) {
        var borrowing = new Borrowing()
                .setId(new ObjectId())
                .setBook(book)
                .setUserId(name)
                .setAt(at == null ? new Date() : at);
        repository.persist(borrowing);
        return repository.findById(borrowing.getId());
    }

    public List<Borrowing> list(String username) {
        return repository.findAllByUsername(username);
    }

    public Borrowing returned(ObjectId borrowingId, Date dateToUse, String username) {
        var currentBorrowing = repository.findById(borrowingId);
        if (currentBorrowing != null && Objects.equals(currentBorrowing.getUserId(), username)) {
            currentBorrowing.setReturnedAt(dateToUse);
            repository.update(currentBorrowing);
            currentBorrowing = repository.findById(borrowingId);
        }
        return currentBorrowing;
    }
}
