package fr.iut.cicd.mapper;

import fr.iut.cicd.domain.Borrowing;
import fr.iut.cicd.dto.BorrowingDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA, uses = ObjectIdMapper.class)
public interface BorrowingMapper {

    BorrowingDTO toDTO(Borrowing domain);

    List<BorrowingDTO> toDTO(List<Borrowing> domain);

    Borrowing toDomain(BorrowingDTO dto);

    List<Borrowing> toDomain(List<BorrowingDTO> dto);
}
