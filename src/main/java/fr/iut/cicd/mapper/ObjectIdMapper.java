package fr.iut.cicd.mapper;

import org.bson.types.ObjectId;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA)
public interface ObjectIdMapper {

    default String toString(ObjectId oid) {
        return oid != null ? oid.toHexString() : null;
    }

    default ObjectId toObjectId(String value) {
        return (value != null && ObjectId.isValid(value)) ? new ObjectId(value) : null;
    }
}
