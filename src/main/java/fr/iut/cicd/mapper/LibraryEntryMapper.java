package fr.iut.cicd.mapper;

import fr.iut.cicd.domain.LibraryEntry;
import fr.iut.cicd.dto.CreateLibraryEntryDTO;
import fr.iut.cicd.dto.LibraryEntryDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA, uses = ObjectIdMapper.class)
public interface LibraryEntryMapper {

    LibraryEntryDTO toDTO(LibraryEntry domain);

    List<LibraryEntryDTO> toDTO(List<LibraryEntry> domain);

    LibraryEntry toDomain(LibraryEntryDTO dto);
    LibraryEntry toDomain(CreateLibraryEntryDTO dto);

    List<LibraryEntry> toDomain(List<LibraryEntryDTO> dto);
}
