package fr.iut.cicd.mapper;

import fr.iut.cicd.domain.Book;
import fr.iut.cicd.dto.BookDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA, uses = ObjectIdMapper.class)
public interface BookMapper {

    BookDTO toDTO(Book domain);

    List<BookDTO> toDTO(List<Book> domain);

    Book toDomain(BookDTO dto);

    List<Book> toDomain(List<BookDTO> dto);
}
