package fr.iut.cicd.mapper;

import fr.iut.cicd.domain.Loan;
import fr.iut.cicd.dto.LoanDTO;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;

import java.util.List;

@Mapper(componentModel = MappingConstants.ComponentModel.JAKARTA, uses = ObjectIdMapper.class)
public interface LoanMapper {

    LoanDTO toDTO(Loan domain);

    List<LoanDTO> toDTO(List<Loan> domain);

    Loan toDomain(LoanDTO dto);

    List<Loan> toDomain(List<LoanDTO> dto);
}
