package fr.iut.cicd.exception;

public class BadParameterException extends RuntimeException {

    private static final String MESSAGE_FORMAT = "Please check %s parameter's value !";

    public BadParameterException(String paramName) {
        super(String.format(MESSAGE_FORMAT, paramName));
    }
}
