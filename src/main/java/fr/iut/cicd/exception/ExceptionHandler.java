package fr.iut.cicd.exception;

import fr.iut.cicd.exception.dto.ErrorDTO;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;

@Provider
@Slf4j
public class ExceptionHandler implements ExceptionMapper<Exception> {

    @Override
    public Response toResponse(Exception e) {
        if (e instanceof BadParameterException
                | e instanceof MandatoryParameterException) {
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity(new ErrorDTO().setError(e.getMessage()))
                    .build();
        } else {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR)
                    .entity(new ErrorDTO().setError(e.getMessage()))
                    .build();
        }
    }
}
