package fr.iut.cicd.exception;

public class MandatoryParameterException extends RuntimeException {

    private static final String MESSAGE_FORMAT = "%s parameter's value must not be empty !";

    public MandatoryParameterException(String paramName) {
        super(String.format(MESSAGE_FORMAT, paramName));
    }
}
