package fr.iut.cicd;

import com.mongodb.client.MongoClient;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.event.Observes;
import jakarta.ws.rs.core.Application;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.openapi.annotations.OpenAPIDefinition;
import org.eclipse.microprofile.openapi.annotations.info.Info;
import org.eclipse.microprofile.openapi.annotations.info.License;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@OpenAPIDefinition(
        tags = {
                @Tag(name = "Borrowings", description = "Borrowings operations."),
                @Tag(name = "Loans", description = "Loans operations."),
                @Tag(name = "Security", description = "Security operations."),
        },
        info = @Info(
                title = "Library Java API",
                version = "1.0.0",
                license = @License(
                        name = "Apache 2.0",
                        url = "https://www.apache.org/licenses/LICENSE-2.0.html")
        )
)
@Slf4j
public class App extends Application {

    void onStart(@Observes StartupEvent ev) {
        // For singleton injection is enough
//        if (ConfigProvider.getConfig().getValue("database-mongodb.migrate-at-start", Boolean.class)) {
//            var databaseName = ConfigProvider.getConfig().getValue("quarkus.mongodb.database", String.class);
//            mongoClient.getDatabase(databaseName)
//                    .listCollectionNames()
//                    .forEach(elt-> log.info("{} collection found !", elt));
//        } else {
//            log.info("Mongo migration is disable at startup");
//        }
        // For @ApplicationScoped beans a client proxy is injected and so we need to invoke some method
        // to force bean instantiation
    }

}