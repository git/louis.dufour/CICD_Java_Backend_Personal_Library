package fr.iut.cicd.controller;


import fr.iut.cicd.dto.CreateLibraryEntryDTO;
import fr.iut.cicd.dto.LibraryEntryDTO;
import fr.iut.cicd.exception.MandatoryParameterException;
import fr.iut.cicd.mapper.LibraryEntryMapper;
import fr.iut.cicd.mapper.ObjectIdMapper;
import fr.iut.cicd.services.LibraryService;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.List;

@Tag(name = "Borrowings")
@Path("/library")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LibraryController {

    @Inject
    LibraryEntryMapper mapper;

    @Inject
    ObjectIdMapper objectIdMapper;

    @Inject
    LibraryService service;

    @Inject
    SecurityIdentity securityIdentity;

    @POST
    @Path("/books")
    public LibraryEntryDTO create(CreateLibraryEntryDTO dto) {
        if (dto == null) {
            throw new MandatoryParameterException("Request body");
        }
        if (dto.getBook() == null || dto.getBook().getId() == null) {
            throw new MandatoryParameterException("book");
        }
        return mapper.toDTO(service.create(mapper.toDomain(dto), getConnectedUsername()));
    }

    @PUT
    @Path("/books/{libraryEntryId}")
    public LibraryEntryDTO create(@PathParam("libraryEntryId") String id, CreateLibraryEntryDTO dto) {
        if (dto == null) {
            throw new MandatoryParameterException("Request body");
        }
        if (dto.getBook() == null || dto.getBook().getId() == null) {
            throw new MandatoryParameterException("book");
        }
        return mapper.toDTO(service.update(mapper.toDomain(dto).setId(objectIdMapper.toObjectId(id)), getConnectedUsername()));
    }

    @DELETE
    @Path("/books/{libraryEntryId}")
    public void delete(@PathParam("libraryEntryId") String id) {
        service.delete(objectIdMapper.toObjectId(id), getConnectedUsername());
    }


    @GET
    @Path("/books")
    public List<LibraryEntryDTO> list() {
        return mapper.toDTO(service.list(getConnectedUsername()));
    }

    private String getConnectedUsername() {
        return securityIdentity.getPrincipal().getName();
    }
}
