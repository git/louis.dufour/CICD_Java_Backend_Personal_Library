package fr.iut.cicd.controller;


import fr.iut.cicd.dto.CreateLoanDTO;
import fr.iut.cicd.dto.EndLoanDTO;
import fr.iut.cicd.dto.LoanDTO;
import fr.iut.cicd.exception.MandatoryParameterException;
import fr.iut.cicd.mapper.BookMapper;
import fr.iut.cicd.mapper.LoanMapper;
import fr.iut.cicd.mapper.ObjectIdMapper;
import fr.iut.cicd.services.LoanService;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.Date;
import java.util.List;

@Tag(name = "Loans")
@Path("/loans")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class LoanController {

    @Inject
    LoanMapper mapper;

    @Inject
    BookMapper bookMapper;

    @Inject
    ObjectIdMapper objectIdMapper;

    @Inject
    LoanService service;

    @Inject
    SecurityIdentity securityIdentity;

    @POST
    public LoanDTO create(CreateLoanDTO dto) {
        if (dto == null) {
            throw new MandatoryParameterException("Request body");
        }
        if (dto.getTo() == null) {
            throw new MandatoryParameterException("to");
        }
        if (dto.getBook() == null || dto.getBook().getId() == null) {
            throw new MandatoryParameterException("book");
        }
        return mapper.toDTO(service.create(bookMapper.toDomain(dto.getBook()), dto.getAt(), dto.getTo(), getConnectedUsername()));
    }

    @POST
    @Path("/{id}/return")
    public LoanDTO returned(@PathParam("id") String borrowingId, EndLoanDTO dto) {
        var dateToUse = dto == null || dto.getReturnedAt() == null ? new Date() : dto.getReturnedAt();
        return mapper.toDTO(service.returned(objectIdMapper.toObjectId(borrowingId), dateToUse, getConnectedUsername()));
    }

    @GET
    public List<LoanDTO> list() {
        return mapper.toDTO(service.list(securityIdentity.getPrincipal().getName()));
    }

    private String getConnectedUsername() {
        return securityIdentity.getPrincipal().getName();
    }
}
