package fr.iut.cicd.controller;

import fr.iut.cicd.dto.FormAuthenticationDTO;
import fr.iut.cicd.services.UserService;
import jakarta.annotation.security.PermitAll;
import jakarta.inject.Inject;
import jakarta.ws.rs.Consumes;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

@Tag(name = "Security")
@PermitAll
@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class SecurityController {


    @Inject
    UserService service;

    @POST
    @Path("/register")
    public void register(FormAuthenticationDTO dto) {
        service.create(dto.getUsername(), dto.getPassword());
    }
//
//    @POST
//    @Path("/login")
//    public TokenDTO login(FormAuthenticationDTO dto) {
//        return new TokenDTO();
//    }
//

}
