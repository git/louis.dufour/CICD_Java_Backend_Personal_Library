package fr.iut.cicd.controller;


import fr.iut.cicd.dto.BorrowingDTO;
import fr.iut.cicd.dto.CreateBorrowingDTO;
import fr.iut.cicd.dto.EndBorrowingDTO;
import fr.iut.cicd.exception.MandatoryParameterException;
import fr.iut.cicd.mapper.BookMapper;
import fr.iut.cicd.mapper.BorrowingMapper;
import fr.iut.cicd.mapper.ObjectIdMapper;
import fr.iut.cicd.services.BorrowingService;
import io.quarkus.security.Authenticated;
import io.quarkus.security.identity.SecurityIdentity;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import org.eclipse.microprofile.openapi.annotations.tags.Tag;

import java.util.Date;
import java.util.List;

@Tag(name = "Borrowings")
@Path("/borrowings")
@Authenticated
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BorrowingController {

    @Inject
    BorrowingMapper mapper;

    @Inject
    BookMapper bookMapper;

    @Inject
    ObjectIdMapper objectIdMapper;

    @Inject
    BorrowingService service;

    @Inject
    SecurityIdentity securityIdentity;

    @POST
    public BorrowingDTO create(CreateBorrowingDTO dto) {
        if (dto == null) {
            throw new MandatoryParameterException("Request body");
        }
        if (dto.getBook() == null || dto.getBook().getId() == null) {
            throw new MandatoryParameterException("book");
        }
        return mapper.toDTO(service.create(bookMapper.toDomain(dto.getBook()), dto.getAt(), getConnectedUsername()));
    }

    @POST
    @Path("/{id}/return")
    public BorrowingDTO returned(@PathParam("id") String borrowingId, EndBorrowingDTO dto) {
        var dateToUse = dto == null || dto.getReturnedAt() == null ? new Date() : dto.getReturnedAt();
        return mapper.toDTO(service.returned(objectIdMapper.toObjectId(borrowingId), dateToUse, getConnectedUsername()));
    }

    @GET
    public List<BorrowingDTO> list() {
        return mapper.toDTO(service.list(securityIdentity.getPrincipal().getName()));
    }

    private String getConnectedUsername() {
        return securityIdentity.getPrincipal().getName();
    }
}
