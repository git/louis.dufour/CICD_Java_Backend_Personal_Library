package fr.iut.cicd.repository;

import fr.iut.cicd.domain.Borrowing;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class BorrowingRepository implements PanacheMongoRepository<Borrowing> {

    public List<Borrowing> findAllByUsername(String username) {
        var params = Parameters.with("username", username);

        return find("userId = :username", params)
                .list();
    }
}
