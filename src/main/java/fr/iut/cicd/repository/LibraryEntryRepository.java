package fr.iut.cicd.repository;

import fr.iut.cicd.domain.LibraryEntry;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class LibraryEntryRepository implements PanacheMongoRepository<LibraryEntry> {

    public List<LibraryEntry> findAllByUsername(String username) {
        var params = Parameters.with("username", username);

        return find("userId = :username", params)
                .list();
    }
}
