package fr.iut.cicd.repository;

import fr.iut.cicd.domain.Loan;
import io.quarkus.mongodb.panache.PanacheMongoRepository;
import io.quarkus.panache.common.Parameters;
import jakarta.enterprise.context.ApplicationScoped;

import java.util.List;

@ApplicationScoped
public class LoanRepository implements PanacheMongoRepository<Loan> {

    public List<Loan> findAllByUsername(String username) {
        var params = Parameters.with("username", username);

        return find("userId = :username", params)
                .list();
    }
}
