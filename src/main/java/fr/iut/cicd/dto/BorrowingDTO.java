package fr.iut.cicd.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class BorrowingDTO extends CreateBorrowingDTO {
    private String id;
    private String userId;
    private Date returnedAt;

}
