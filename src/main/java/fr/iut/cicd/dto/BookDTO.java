package fr.iut.cicd.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class BookDTO {
    @Schema(required = true)
    private String id;
    private String title;
    private List<String> publishers;
    private Date publishDate;
    private String isbn13;
    private List<String> series;
    private Integer nbPages;
    private String imageSmall;
    private String imageMedium;
    private String imageLarge;
}
