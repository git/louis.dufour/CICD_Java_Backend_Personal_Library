package fr.iut.cicd.dto;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class CreateLibraryEntryDTO {
    private BookDTO book;

}
