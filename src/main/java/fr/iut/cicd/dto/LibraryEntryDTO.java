package fr.iut.cicd.dto;

import fr.iut.cicd.domain.Book;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class LibraryEntryDTO {
    private String id;
    private BookDTO book;

}
