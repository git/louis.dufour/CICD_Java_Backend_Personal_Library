package fr.iut.cicd.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Date;

@Data
@Accessors(chain = true)
public class CreateLoanDTO {
    @Schema(required = true)
    private BookDTO book;

    @Schema(required = true, description = "User name of the borrower.")
    private String to;

    private Date at;

}
