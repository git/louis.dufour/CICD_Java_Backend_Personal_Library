package fr.iut.cicd.dto;

import lombok.Data;
import lombok.experimental.Accessors;
import org.eclipse.microprofile.openapi.annotations.media.Schema;

import java.util.Date;

@Data
@Accessors(chain = true)
public class EndBorrowingDTO {
    @Schema(description = "Today if not filled.")
    private Date returnedAt;

}
