DROP TABLE IF EXISTS users;

CREATE TABLE IF NOT EXISTS users (
    id SERIAL NOT NULL PRIMARY KEY,
    username VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    role VARCHAR(255) NOT NULL
);

INSERT INTO users (id, username, password, role) VALUES
(1, 'bob', 'bob', 'user');

alter table if exists users alter column id set data type bigint;

DROP SEQUENCE IF EXISTS users_SEQ;

create sequence users_SEQ start with 2 increment by 50;
