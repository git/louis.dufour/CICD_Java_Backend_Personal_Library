package fr.iut.cicd.services;


import fr.iut.cicd.domain.Book;

import java.util.Arrays;
import java.util.List;

public class TestData {

    public static Book BOOK_1 = new Book()
            .setId("drklmjgkznbl")
            .setNbPages(210)
            .setPublishers(Arrays.asList("Jean", "Jacques"))
            .setTitle("The book")
            .setIsbn13("e45frze045e22");


    public static Book BOOK_2 = new Book()
            .setId("gkenrlkaùbn")
            .setNbPages(42)
            .setPublishers(List.of("Michel"))
            .setTitle("The book, the return")
            .setIsbn13("45e04er5gh014e");
}
