package fr.iut.cicd.services;

import fr.iut.cicd.domain.LibraryEntry;
import fr.iut.cicd.repository.LibraryEntryRepository;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.h2.H2DatabaseTestResource;
import io.quarkus.test.junit.QuarkusTest;
import io.quarkus.test.junit.mockito.InjectSpy;
import jakarta.inject.Inject;
import org.bson.types.ObjectId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static fr.iut.cicd.services.TestData.BOOK_1;
import static fr.iut.cicd.services.TestData.BOOK_2;
import static io.smallrye.common.constraint.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertEquals;

@QuarkusTest
@QuarkusTestResource(H2DatabaseTestResource.class)
public class LibraryServiceTest {

    @Inject
    LibraryService libraryService;

    @InjectSpy
    LibraryEntryRepository repository;

    @BeforeEach
    void setUp() {
        Mockito.doReturn(
                        Arrays.asList(
                                new LibraryEntry().setId(new ObjectId()).setUserId("Toto").setBook(BOOK_1),
                                new LibraryEntry().setId(new ObjectId()).setUserId("Toto").setBook(BOOK_2)
                        ))
                .when(repository).findAllByUsername("Toto");
    }

    @Test
    void whenFindForUser_thenBooksShouldBeFound() {
        assertEquals(2, libraryService.list("Toto").size());
    }

    @Test
    void addABookToAnUserLibrary_thenShouldBeOk() {
        var libEntry = new LibraryEntry()
                .setBook(BOOK_1);
        Mockito.doAnswer(invocationOnMock -> invocationOnMock.getArgument(0))
                .when(repository)
                .persist(libEntry);

        var result = libraryService.create(libEntry, "Toto");
        assertEquals("Toto", result.getUserId());
        assertNotNull(result.getId());
    }
}
